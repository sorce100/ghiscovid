@extends('layouts.default')
@section('title','GHIS-COVID-19')
@section('mainContent')
    <!-- where the man contet goes -->

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="row x_title">
                    <div class="col-md-6">
                        <h3><i class="fa fa-road"></i> Contacts Movement Module </h3>
                    </div>
                    <div class="col-md-6">
                        <div class="pull-right">
                            <div class="pull-right"><button class="btn btn-danger" data-toggle="modal" data-target="#addContactMovementModal" data-backdrop="static" data-keyboard="false">Add New <i class="fa fa-plus"></i></button></div>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                        <table class="table table-striped jambo_table tableList projects">
                            <thead>
                                <tr>
                                    <th>EPID Number</th>
                                    <th>Contact Name</th>
                                    <th>Contact Sex</th>
                                    <th>Case Classification</th>
                                    <th>Status</th>
                                    <th>Added</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>GHS101</td>
                                    <td>
                                        <a> Sorce Kwarteng</a>
                                        <br />
                                        <small>Created 06/04/20</small>
                                    </td>
                                    <td>
                                        Male
                                    </td>
                                    <td class="project_progress">
                                        Negative
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-xs">Negative</button>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>GHS101</td>
                                    <td>
                                        <a> Sorce Kwarteng</a>
                                        <br />
                                        <small>Created 06/04/20</small>
                                    </td>
                                    <td>
                                        Male
                                    </td>
                                    <td class="project_progress">
                                        Negative
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-xs">Negative</button>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  -->

    <!-- end of main content -->
@endsection