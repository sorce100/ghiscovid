<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// for index page
Route::get('/', function () {
    return view('index');
});
// for login page
Route::get('/login', function () {
    return view('login');
});
// for main admin dashboard
Route::get('/dashboard', function () {
    return view('admin.dashboard');
});

Route::get('/caseinvestigation', function () {
    return view('admin.caseinvestigation');
});
Route::get('/contactidentification', function () {
    return view('admin.contactidentification');
});
Route::get('/contactmovement', function () {
    return view('admin.contactmovement');
});
