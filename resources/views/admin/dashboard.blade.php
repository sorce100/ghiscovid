@extends('layouts.default')
@section('title','GHIS-COVID-19')
@section('mainContent')
    <!-- where the man contet goes -->

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <div class="row tile_count">
                        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                            <span class="count_top"><i class="fa fa-user"></i> Total Contacts</span>
                            <div class="count">2500</div>
                            <span class="count_bottom"> From Yesterday</span>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                            <span class="count_top"><i class="fa fa-road"></i> Total Tracing</span>
                            <div class="count">123.50</div>
                            <span class="count_bottom">From Yesterday</span>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                            <span class="count_top"><i class="fa fa-plus"></i> Positive Contacts</span>
                            <div class="count green">2,500</div>
                            <span class="count_bottom"> From Yesterday</span>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                            <span class="count_top"><i class="fa fa-exclamation-triangle"></i> Critical Condition</span>
                            <div class="count">4,567</div>
                            <span class="count_bottom">From Yesterday</span>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                            <span class="count_top"><i class="fa fa-user"></i> Total Recoveries</span>
                            <div class="count blue">2,315</div>
                            <span class="count_bottom"> From Yesterday</span>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                            <span class="count_top"><i class="fa fa-user"></i> Total Fatalities</span>
                            <div class="count red">7,325</div>
                            <span class="count_bottom"> From Yesterday</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  -->
    
    
    <!-- end of main content -->
@endsection