@extends('layouts.default')
@section('title','GHIS-COVID-19')
@section('mainContent')
    <!-- where the man contet goes -->

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="row x_title">
                    <div class="col-md-6">
                        <h3><i class="fa fa-search"></i> Contacts Investigation Module </h3>
                    </div>
                    <div class="col-md-6">
                        <div class="pull-right">
                            <div class="pull-right"><button class="btn btn-danger" data-toggle="modal" data-target="#addContactInvestigationModal" data-backdrop="static" data-keyboard="false">Add New <i class="fa fa-plus"></i></button></div>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <div class="table-responsive">
                    <table class="table table-striped jambo_table tableList projects">
                            <thead>
                                <tr>
                                    <th>EPID Number</th>
                                    <th>Contact Name</th>
                                    <th>Contact Sex</th>
                                    <th>Case Classification</th>
                                    <th>Status</th>
                                    <th>Added</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>GHS101</td>
                                    <td>
                                        <a> Sorce Kwarteng</a>
                                        <br />
                                        <small>Created 06/04/20</small>
                                    </td>
                                    <td>
                                        Male
                                    </td>
                                    <td class="project_progress">
                                        Negative
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-xs">Negative</button>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>GHS101</td>
                                    <td>
                                        <a> Sorce Kwarteng</a>
                                        <br />
                                        <small>Created 06/04/20</small>
                                    </td>
                                    <td>
                                        Male
                                    </td>
                                    <td class="project_progress">
                                        Negative
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-xs">Negative</button>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> View </a>
                                        <a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                        <a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  -->

    <!-- end of main content -->
@endsection

<div class="modal fade" id="addContactInvestigationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h4 class="modal-title con_investigate_title">Add Contact Investigation</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="pages_form">
            <div class="row">
                <div class="col-md-12">
                    <!-- 1 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">EPID NUMBER <span class="asterick"> *</span></label>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                               <input type="text" name="con_epid_num" id="con_epid_num" class="form-control" placeholder="" autocomplete="off" required>
                            </div>
                        </div>
                        <!--  -->
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">WHO Case ID <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                               <input type="text" name="con_who_num" id="con_who_num" class="form-control" placeholder="" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <!-- 2 -->

                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                      <button type="submit" class="btn btn-primary" >Save  <i class="fa fa-save"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->