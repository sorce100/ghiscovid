<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1, user-scalable=no">
    <link rel="icon" type="image/jpg" sizes="96x96" href="images/coatofarms.jpg">
    <title>GHIS-COVID-19</title>
    <!-- <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Nunito" /> -->
    <link rel="icon" type="image/jpg" sizes="96x96" href="images/coatofarms.jpg">
    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <!-- Custom Theme Style -->
    <link href="build/css/custom.css" rel="stylesheet">

    <!-- map css ans js -->
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.8.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v1.8.1/mapbox-gl.css' rel='stylesheet' />

  </head>

  <body class="nav-md footer_fixed">
      
    <div class="container-fluid body">
        <!-- main  row -->
        <div class="row">
            <!-- first col -->
            <div class="col-xs-3 col-md-3 col-lg-3">
                <div class="row">
                    <!--  -->
                    <div>
                        <div class="x_panel displayCards">
                            <div class="x_content">
                                <div class="dashboard-widget-content">
                                    <div class="col-sm-12 col-md-12 col-lg-12 coatofarms">
                                    <!-- content of card -->
                                    <img src="images/coatofarms.jpg" class="img img-responsive" width="50%" alt="coat of arms of ghana">
                                    <img src="images/ghis_logo.jpg" class="img img-responsive" width="50%" alt="ghis logo ghana">
                                    <!-- end of content of card -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </div>
                <div class="row">
                    <!--  -->
                    <div>
                        <div class="x_panel displayCards">
                            <div class="x_content">
                                <div class="dashboard-widget-content">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                    <!-- content of card -->
                                    <h1 class="text-center cardh1 bold">98,34394</h1>
                                    <h1 class="text-center bold">Total Test</h1>
                                    <hr>
                                    <h2 class="text-center cardh2 bold">98,34394</h2>
                                    <h2 class="text-center bold">Public Labs</h2>
                                    <hr>
                                    <h2 class="text-center cardh2 bold">98,34394</h2>
                                    <h2 class="text-center bold">Private Labs</h2>
                                    <!-- end of content of card -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </div>
                <div class="row">
                    <!--  -->
                    <div>
                        <div class="x_panel displayCards">
                            <div class="x_content">
                                <div class="dashboard-widget-content">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                    <!-- content of card -->
                                    <h1 class="text-center casesReported bold">98,34394</h1>
                                    <h1 class="text-center bold">Cases Reported</h1>
                                    <!-- end of content of card -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </div>
                <div class="row">
                    <!--  -->
                    <div>
                        <div class="x_panel displayCards">
                            <div class="x_content">
                                <div class="dashboard-widget-content">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                    <!-- content of card -->
                                    <h1 class="text-center fatalities casesReported bold">98,34394</h1>
                                    <h1 class="text-center bold">Fatalities</h1>
                                    <!-- end of content of card -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </div>
            </div>
            <!-- second col -->
            <div class="col-xs-6 col-md-6 col-lg-6">
                <div class="row">
                    <div>
                        <div class="x_panel displayCards">
                            <div class="x_content">
                                <div class="dashboard-widget-content">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                    <!-- content of card -->
                                    <div id='map'></div>
                                    <div id='console'>
                                        <h1>GHANA REGIONAL COVID-19</h1>
                                        <p>Data: <a href='#'>Ministry of Health - 2020</p>

                                        <div class='session'>
                                            <h2>Select Region To display Stats</h2>
                                            <div class='row' id='filters'>
                                                <input id='all' type='radio' name='toggle' value='all' checked='checked'>
                                                <label for='all'>All</label>
                                                <br>
                                                <input id='weekday' type='radio' name='toggle' value='weekday'>
                                                <label for='weekday'>Greater Accra Region</label>
                                                <br>
                                                <input id='weekend' type='radio' name='toggle' value='weekend'>
                                                <label for='weekend'>Ashanti Region</label>
                                                <br>
                                                <input id='weekend' type='radio' name='toggle' value='weekend'>
                                                <label for='weekend'>Norther Region </label>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <button id="fit">Fit to Ghana</button> -->
                                    <!-- end of content of card -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  -->
                <div class="row">
                    <div>
                        <div class="x_panel displayCards">
                            <div class="x_content">
                                <div class="dashboard-widget-content">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                    <!-- content of card -->
                                    <h3>Disclaimer: All data are provisional and subject to change.</h3>
                                    <h3>This dashboard will be updated daily by 12 Noon. Data displayed are current as of 8pm for the prior day.</h3>
                                    <h3>Data produced by the Ministry of health.</h3>
                                    <!-- end of content of card -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- third col -->
            <div class="col-xs-3 col-md-3 col-lg-3">
                <!-- row 1 -->
                <div class="row">
                    <!--  -->
                    <div>
                        <div class="x_panel displayCards">
                            <div class="x_content">
                                <div class="dashboard-widget-content">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                    <!-- content of card -->
                                    <h1 class="text-center cardh1 bold">5 of 16</h1>
                                    <h1 class="text-center bold">Regions Reporting Cases</h1>
                                    <!-- end of content of card -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </div>
                <!-- row 2 -->
                <div class="row">
                    <!--  -->
                    <div>
                        <div class="x_panel displayCards">
                            <div class="x_content">
                                <div class="dashboard-widget-content">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                    <!-- content of card -->
                                    <ul class="list-group">
                                        <li class="list-group-item"> 
                                            <span class="glyphicon glyphicon-globe"></span> Greater Accra Region
                                        </li>
                                        <li class="list-group-item">
                                            <span class="glyphicon glyphicon-globe"></span> Central Region
                                        </li>
                                        <li class="list-group-item">
                                            <span class="glyphicon glyphicon-globe"></span> Eastern Region
                                        </li>
                                        <li class="list-group-item">
                                            <span class="glyphicon glyphicon-globe"></span> Western Region
                                        </li>
                                        <li class="list-group-item">
                                            <span class="glyphicon glyphicon-globe"></span> Western North Region
                                        </li>
                                        <li class="list-group-item">
                                            <span class="glyphicon glyphicon-globe"></span> Ashanti Region
                                        </li>
                                        <li class="list-group-item">
                                            <span class="glyphicon glyphicon-globe"></span> Volta Region
                                        </li>
                                        <li class="list-group-item">
                                            <span class="glyphicon glyphicon-globe"></span> Oti Region
                                        </li>
                                        <li class="list-group-item">
                                            <span class="glyphicon glyphicon-globe"></span> Northern Region
                                        </li>
                                        <li class="list-group-item">
                                            <span class="glyphicon glyphicon-globe"></span> Savannah Region
                                        </li>
                                        <li class="list-group-item">
                                            <span class="glyphicon glyphicon-globe"></span> North East Region
                                        </li>
                                        <li class="list-group-item">
                                            <span class="glyphicon glyphicon-globe"></span> Upper East Region
                                        </li>
                                        <li class="list-group-item">
                                            <span class="glyphicon glyphicon-globe"></span> Upper West Region
                                        </li>
                                        <li class="list-group-item">
                                            <span class="glyphicon glyphicon-globe"></span> Bono Region
                                        </li>
                                        <li class="list-group-item">
                                            <span class="glyphicon glyphicon-globe"></span> Bono-East Region
                                        </li>
                                        <li class="list-group-item">
                                            <span class="glyphicon glyphicon-globe"></span> Ahafo Region
                                        </li>

                                    </ul>
                                    <!-- end of content of card -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </div>
                <!-- row 3 -->
                <div class="row">
                    <!--  -->
                    <div>
                        <div class="x_panel displayCards">
                            <div class="x_content">
                                <div class="dashboard-widget-content">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                    <!-- content of card -->
                                    <h1 class="text-center bold">Last Updated </h1>
                                    <h1 class="text-center cardh1 bold">6/5/2020 11:00 PM</h1>
                                    <!-- end of content of card -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                </div>
            </div>
        </div>

    </div>

    <script src="../build/js/index.js"></script>
    <!-- jQuery -->
    <!-- <script src="../vendors/jquery/dist/jquery.min.js"></script> -->
    <!-- Bootstrap -->
    <!-- <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script> -->
    <!-- FastClick -->
    <!-- <script src="../vendors/fastclick/lib/fastclick.js"></script> -->
    <!-- NProgress -->
    <!-- <script src="../vendors/nprogress/nprogress.js"></script> -->
    <!-- jQuery custom content scroller -->
    <!-- <script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script> -->

    <!-- Custom Theme Scripts -->
    <!-- <script src="../build/js/custom.js"></script> -->
    
    
    <!-- map -->
    
  </body>
</html>