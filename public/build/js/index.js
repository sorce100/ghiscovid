mapboxgl.accessToken = 'pk.eyJ1Ijoic29yY2UiLCJhIjoiY2s4b2RvMW00MDBzZjNmbzkzZG11YnFxeCJ9.5983tVuRbi7uqGdQtK-bZg';
var map = new mapboxgl.Map({
container: 'map',
style: 'mapbox://styles/mapbox/dark-v10',
center: [-1.0307118, 7.9527706], // starting position
zoom: 6 // starting zoom
});
// for fullscreen
map.addControl(new mapboxgl.FullscreenControl());
// locate user of map
// Add geolocate control to the map.
map.addControl(
    new mapboxgl.GeolocateControl({
        positionOptions: {
        enableHighAccuracy: true
        },
        trackUserLocation: true
    })
);

document.getElementById('fit').addEventListener('click', function() {
    map.fitBounds([
    [32.958984, -5.353521],
    [43.50585, 5.615985]
    ]);
});